'''Exports the interfaces from the tiles blend.

Of the format:
{
    'root_obj_name': {
        'ground_objs':[ground_obj_names],
        'entry_interface':'A',
        'exit_interfaces: {
            'name':'obj_name'
            'interface': 'B'
        }
    }

}

'''
import bpy
import json
import os

JSON_PARAMS = {'sort_keys': True, 'separators': (',', ':')}

project_dir = os.path.split(os.path.realpath(__name__))[0]
out_path = os.path.join(project_dir, 'bin', 'tile_data.json')

def start(obj_list):
    obj_list = filter_only_roots(obj_list)

    out_dict = dict()
    for obj in obj_list:
        out_dict[obj.name] = InterfaceTree(obj)
    json.dump(out_dict, open(out_path, 'w'), **JSON_PARAMS)

def filter_only_roots(obj_list):
    '''Returns a list only of the objects that are the parent of a passage tree
    '''
    root_objs = list()
    for obj in obj_list:
        if obj.parent is None:
            if 'interface' in obj.game.properties:
                root_objs.append(obj)
    return root_objs

class InterfaceTree(dict):
    def __init__(self, tree_root):
        super().__init__()
        self.obj = tree_root
        self.name = tree_root.name
        self['ground_objs'] = [o.name for o in get_children_with_property(self.obj, 'GROUND')]
        self['entry_interface'] = self.obj.game.properties['interface'].value

        self['exit_interfaces'] = self.get_exit_interfaces()

    def get_exit_interfaces(self):
        out_list = list()
        for child in get_children_with_property(self.obj, 'interface'):
            out_list.append({
                'name': child.name,
                'interface': child.game.properties['interface'].value
            })
        return out_list

def get_children_with_property(obj, property_name):
    child_list = list()
    for child in obj.children:
        if property_name in child.game.properties:
            child_list.append(child)
    return child_list

if __name__ == "__main__":
    start(bpy.data.objects)
