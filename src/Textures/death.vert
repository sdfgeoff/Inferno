attribute vec3 aPosition;
attribute vec2 aUv0;
attribute vec4 aColor;

uniform mat4   matrix_viewProjection;
uniform mat4   matrix_model;

varying vec2 UVpos;
varying vec4 vertexColor;

void main(void)
{
    UVpos = aUv0.xy;
    vertexColor = aColor;
    gl_Position = matrix_viewProjection * matrix_model * vec4(aPosition, 1.0);
}
