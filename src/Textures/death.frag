
uniform sampler2D uDiffuseMap;
uniform float level;

varying vec2 UVpos;
varying vec4 vertexColor;

#define MAIN_COLOR vec3(0.0, 1.0, 1.0)

void main(void)
{
    float noise = texture2D(uDiffuseMap, UVpos).r;
    noise = noise * 2.0;
    noise += vertexColor.r;
    noise = noise / 2.0;
    noise = level*3.0 - noise;
    vec3 vec_noise = vec3(noise);

    //Pegtop's Formula
    vec3 out_col = MAIN_COLOR * vec_noise + vec_noise;


    gl_FragColor.rgb = out_col;
    gl_FragColor.a = 1.0;
}
