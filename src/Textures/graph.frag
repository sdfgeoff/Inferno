
uniform sampler2D uDiffuseMap;
uniform float level;

varying vec2 UVpos;
varying vec4 vertexColor;


void main(void)
{
    float graph_static = UVpos.x;
    if (graph_static > level){
        graph_static = 1.0;
    } else {
        graph_static = 0.0;
    }
    graph_static = pow(graph_static, 2.2);
    gl_FragColor = vertexColor * graph_static;
}
