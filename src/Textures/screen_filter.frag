uniform sampler2D uColorBuffer;
uniform sampler2D uDepthBuffer;

#define STREAKS 1000.0

uniform float streak_length;
uniform float zoom_amt;
uniform float chromatic_abbr;
uniform float fisheye_amt;
uniform float streak_center_y;
uniform float streak_center_x;
varying vec2 vUv0;

uniform float streak_offset;

float rand(float num){
    return 0.5 + 0.5 * fract(sin(num * 78.233) * 437585.5453);
}

float getangle(vec2 coords){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    return float(int(atan(normalized.x / normalized.y) * STREAKS));
}

vec2 noisyzoom(vec2 coords, float zoom, float depth){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    float dist = length(normalized);

    // Streaks:
    float streaks = rand(getangle(coords)+streak_offset);
    streaks *= pow(dist, 2.0);
    streaks *= streak_length;
    streaks *= depth;
    normalized *= 1.0 - streaks;

    // Zoom:
    normalized *= zoom;

    return normalized + vec2(streak_center_x, streak_center_y);
}


vec2 fisheye(vec2 coords){
    vec2 normalized = coords - vec2(0.5, 0.5);
    float dist = length(normalized);
    return normalized * (dist + fisheye_amt) / (fisheye_amt + 1.0) + vec2(0.5, 0.5);
}

float unpackFloat(vec4 rgbaDepth){
    const vec4 bitShift = vec4(1.0 / (256.0 * 256.0 * 256.0), 1.0 / (256.0 * 256.0), 1.0 / 256.0, 1.0);
    return dot(rgbaDepth, bitShift);
}

void main(void){
    vec2 orig_coord = vUv0;

    orig_coord = fisheye(orig_coord);

    //float depth = clamp(1.0 - pow(texture2D(uDepthBuffer, orig_coord).x, 4.0), 0.0, 1.0);
    float depth = pow(unpackFloat(texture2D(uDepthBuffer, orig_coord)), 0.2);
    float close = clamp((1.0 - depth*2.2), 0.0, 1.0);

    vec2 texcoord = noisyzoom(orig_coord, zoom_amt, close);
    gl_FragColor.r = texture2D(uColorBuffer, texcoord).r;// + (depth);

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr, close);
    gl_FragColor.g = texture2D(uColorBuffer, texcoord).g;

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr * 2.0, close);
    gl_FragColor.b = texture2D(uColorBuffer, texcoord).b;

    gl_FragColor.a = 1.0;
}
