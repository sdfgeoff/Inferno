pc.extend(pc, function () {
    var SpeedEffect = function (graphicsDevice) {
        // User Accessible Uniforms:
        this.streak_length = 3.0
        this.zoom_amt = 1.0
        this.fisheye_amt = 2.0
        this.chromatic_abbr = 0.00
        this.streak_center_y = 0.5
        this.streak_center_x = 0.5

        //Private uniform:
        this._streak_offset = 0.0

        //Shaders metagumphf:
        this.needsDepthBuffer = true;
        var attributes = {
            aPosition: pc.SEMANTIC_POSITION
        };

        var passThroughVert = <<STRING src/Textures/screen_filter.vert>>

        var luminosityFrag = "precision " + graphicsDevice.precision + " float;\n" +
            <<STRING src/Textures/screen_filter.frag>>

        this.speedShader = new pc.Shader(graphicsDevice, {
            attributes: attributes,
            vshader: passThroughVert,
            fshader: luminosityFrag
        });
    };

    SpeedEffect = pc.inherits(SpeedEffect, pc.PostEffect);

    SpeedEffect.prototype = pc.extend(SpeedEffect, {
        render: function (inputTarget, outputTarget, rect) {
            var device = this.device;
            var scope = device.scope;

            scope.resolve("uColorBuffer").setValue(inputTarget.colorBuffer);
            scope.resolve("uDepthBuffer").setValue(this.depthMap);
            scope.resolve("streak_length").setValue(this.streak_length);
            scope.resolve("zoom_amt").setValue(this.zoom_amt);
            scope.resolve("fisheye_amt").setValue(this.fisheye_amt);
            scope.resolve("chromatic_abbr").setValue(this.chromatic_abbr);
            scope.resolve("streak_center_y").setValue(this.streak_center_y);
            scope.resolve("streak_center_x").setValue(this.streak_center_x);
            scope.resolve("streak_offset").setValue(this._streak_offset);
            this._streak_offset += 0.1

            pc.drawFullscreenQuad(device, outputTarget, this.vertexBuffer, this.speedShader, rect);
        }
    });

    return {
        SpeedEffect: SpeedEffect
    };
}());
