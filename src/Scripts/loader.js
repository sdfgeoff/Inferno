function InfernoLoader(hud){
    /* This is some abstraction to do with loading.
    The first part is loading scripts. This is done with:
        InfernoLoader.load_scripts(url_list, callback)
    They are not loaded sequentially
    The second part runs functions sequentially. Each is passet three arguments:
     - The loader (so you can use load_url)
     - A function to run with ("display name", percent)
     - A function to run when loading is complete
    */
    "use strict"
    var self = this
    self.onloaded = null  // Callback to run when loaded

    self._load_id = 0
    self.load_list = []  // List of functions to call as part of loading.

    self._script_load_counter = 0
    self._script_load_callback = null
    self._script_total_count = 0
    self._script_load_name = ''
    self.hud = hud

    self.start_load = function(){
        hud.setBigText("Starting Load")
        self._load_next()
    }

    self._load_next = function(){
        if (self._load_id < self.load_list.length){
            self._load_id += 1  // Needs to be done before running to prevent
                                // Infinite loop
            var funct = self.load_list[self._load_id - 1]
            funct(self, self._update_load_screen, self._load_next)
            console.log("Loading "+self._load_id+'/'+self.load_list.length)
            hud.setBigText("Loading "+self._load_id+'/'+self.load_list.length)
        } else {
            hud.setBigText("Loading Complete")
            self.onloaded()
        }
    }



    self.load_url = function(url, callback){
        var xhr = new XMLHttpRequest()
        xhr.addEventListener("load", function (){
            callback(this.response)
        })
        xhr.open("GET", url)
        xhr.send()
    }

    self.load_scripts = function(name, url_list, callback){
        //Loads a list of scripts
        self._script_load_counter = 0
        self._script_total_count = url_list.length
        self._script_load_name = name

        self._script_load_callback = callback
        for (var url_id in url_list){
            var url = url_list[url_id]
            var script = document.createElement('script');
            document.body.appendChild(script)
            script.onload = self._on_script_loaded;
            script.src = 'Scripts/'+url;
        }
    }
    self._on_script_loaded = function(){
        // Runs whenever a script has been loaded. Runs a script_load callback
        // when all scripts have been loaded
        self._script_load_counter += 1
        self._update_load_screen(self._script_load_name, self._script_load_counter/self._script_total_count)
        if (self._script_load_counter === self._script_total_count){
            self._script_load_callback()
        }
    }

    self._update_load_screen = function(name, percent){
        console.log("Loading "+name+" "+Math.floor(percent*100) + "%")
        hud.setBigText("Loading "+name+" "+Math.floor(percent*100) + "%")
    }
}

function InfernoObjectListLoader(app, name, url_list, percent_callback, done_callback){
    // Loads a list of object URL's into playcanvas and co-operates nicely
    // with the Loader
    "use strict"
    var self = this
    self.name = name
    self.app = app
    self.url_list = url_list
    self.percent_callback = percent_callback
    self.done_callback = done_callback

    self._num_assets = 0
    self._asset_load_counter = 0


    self.start = function(){
        // Starts it loading....
        self.percent_callback(self.name, 0)

        for (var i = 0; i < self.url_list.length; i++) {
            // Load it
            var url = "Meshes/"+self.url_list[i]+".json";
            self.app.assets.loadFromUrl(url, "model", self._on_asset_load)
            self._num_assets += 1
        }
        if (self.url_list.length === 0){
            self.done_callback()
        }
    }

    self._on_asset_load = function(err, asset){
        // Runs when each asset loads
        self._asset_load_counter += 1
        self.percent_callback(self.name, self._asset_load_counter / self._num_assets)
        if (self._asset_load_counter === self._num_assets){
            self.done_callback()
        }
    }



}
