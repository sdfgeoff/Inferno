/* globals pc: true */
/* globals InfernoLoader: true */

function Inferno(){
    "use strict"
    var self = this

    self.RESET_STATE = 0
    self.WAIT_CLICK_STATE = 1
    self.STARTUP_STATE = 2
    self.PLAYING_STATE = 3
    self.DEATH_STATE = 4

    self.state = self.RESET_STATE

    self.init = function(canvas, hud){
        self.canvas = canvas;
        self.playing = false;
        self.hud = new InfernoTextHUD(hud)
        self.loader = new InfernoLoader(self.hud)
        self.loader.load_scripts(
            "Engine",
            [
                'ammo.js',
                'playcanvas-latest.js',
            ],
            self.load_other_scripts // This function get's run when these files have loaded
        )
    }

    self.update = function(dt){
        // This function runs every single frame, regardless of state. It handles
        // the state machine.
        if (self.state === self.RESET_STATE){
            self._reset_state(dt)
        } else if (self.state === self.WAIT_CLICK_STATE){
            // No update function because uses callback
        } else if (self.state === self.STARTUP_STATE){
            self._startup_state(dt)
        } else if (self.state === self.PLAYING_STATE){
            self._play_state(dt)
        } else if (self.state === self.DEATH_STATE){
            self._death_state(dt)
        }
    }

    self._reset_state = function(dt){
        //Starts a new runthrough of the game
        self.map.reset()
        self.player.reset()

        self.map.init()
        self.player.init()
        self.hud.setBigText("Click to begin")
        self.app.mouse.on(pc.EVENT_MOUSEDOWN, self.wait_for_mouse);
        self.state = self.WAIT_CLICK_STATE
    }

    self.wait_for_mouse = function(){
        //Runs while waiting for the player to click the screen
        self.app.mouse.off(pc.EVENT_MOUSEDOWN, self.wait_for_mouse)
        self.start_time = self.app._time
        self.state = self.STARTUP_STATE
    }

    self._startup_state = function(){
        // Starts the game playing with a little wind-in animation
        var time_since_start = self.app._time - self.start_time
        var objs = self.player.extras.hud.objs

        if (time_since_start > 100){
            self.player.boost.transaction(5 / 60)
        }
        if (time_since_start > 400){
            self.player.slow.transaction(5 / 60)
        }
        if (time_since_start > 700){
            self.player.shield.transaction(0.3 / 120)
        }

        self.flicker(objs.HelpTextTimeWarp, (2800 - time_since_start)/1000)
        self.flicker(objs.HelpTextBoost, (3000 - time_since_start)/1000)
        self.flicker(objs.HelpTextShield, (3500 - time_since_start)/1000)

        if (time_since_start > 1000 && time_since_start < 1500){
            self.hud.setBigText("3")
        } else if (time_since_start > 2000 && time_since_start < 2500){
            self.hud.setBigText("2")
        } else if (time_since_start > 3000 && time_since_start < 3500){
            self.hud.setBigText("1")
        } else if (time_since_start > 4000.0){
            self.hud.setBigText("GO")
            self.state = self.PLAYING_STATE
        }

        var death_level = self.player.extras.hud.get_death_plane_level()
        self.player.extras.hud.set_death_plane_level(death_level * 0.95)
    }



    self._play_state = function(dt){
        self.player.update(dt)
        if (self.player.dead){
            self.state = self.DEATH_STATE
        }
    }

    self._death_state = function(dt){
        self.player.update(dt)
        var death_level = self.player.extras.hud.get_death_plane_level()
        self.player.extras.hud.set_death_plane_level(death_level * 0.95 + 1.0 * 0.05)
        if (death_level > 0.9999){//Ghetto timer for death
            self.hud.clear()
            self.state = self.RESET_STATE
        }
    }

    // --------------------------- LOADING FUNCTIONS ---------------------------
    self.load_other_scripts = function(){
        self.loader.load_scripts(
            "Scripts",
            [
                'map.js',
                'player.js',
                'camera.js',
                'filter.js',
                'bank.js'
            ],
            self.scripts_loaded
        )
    }

    self.scripts_loaded = function(){
        //Runs when all the scripts have loaded
        // Create the app and start the update loop
        self.app = new pc.Application(self.canvas)
        self.app.mouse = new pc.Mouse(self.canvas)
        self.app.mouse.disableContextMenu()

        // Set the canvas to fill the window and automatically change resolution to be the same as the canvas size
        window.addEventListener("resize", self.on_resize);
        self.on_resize()

        self.app.scene.ambientLight = new pc.Color(0, 0, 0)

        //Make the various object instances
        self.map = new Inferno.types.Map(self.app)
        self.player = new Inferno.types.Player(self.app, self.map, self.hud)

        //Configure the loader...
        self.loader.load_list.push(self.player.extras.load)
        self.loader.load_list.push(self.map.load)
        self.loader.onloaded = self.on_loaded

        self.loader.start_load()
    }

    self.on_loaded = function(){
        // Runs when the loader has finished to start the game actually playing
        self.app.start();
        // Disabling gravity has to happen AFTER the app has started
        self.app.systems.rigidbody.setGravity(0, 0, 0)

        self.app.scene.ambientLight.b = 0
        self.app.scene.ambientLight.r = 0
        self.app.scene.ambientLight.g = 0
        self.app.on('update', self.update)
        self.app.mouse.on(pc.EVENT_MOUSEDOWN, self.lock_mouse);
    }



    // ------------------------ SUPPORT FUNCTIONS ------------------------------
    self.flicker = function(obj, probability){
        // Makes an object flash with a specified probability
        obj.visible = Math.random() < probability
    }

    self.on_resize = function(){
        // Resizes the canvas to be full screen
        var w = self.canvas.innerWidth
        var h = self.canvas.innerHeight
        self.app.setCanvasFillMode(pc.FILLMODE_FILL_WINDOW)
        self.app.setCanvasResolution(pc.RESOLUTION_AUTO, w, h)
    }

    self.lock_mouse = function(){
        console.log("Mouse Locked")
        self.app.mouse.enablePointerLock()
    }


}
Inferno.types = {}
