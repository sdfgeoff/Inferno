/* globals Inferno: true */

Inferno.types.Bank = function(initial_reserve, min_cap, max_cap){
    "use strict"
    var self = this
    /*Stores cash - er, or whatever.

    Takes the initial amount when initilizing as well as optionally a min or
    max amount (cap) that can be deposited. The cap can be None in which case
    the bank can accept infinite money (well, 10^300 or so if it's a float)

    All transactions should be done with the transaction method, which will
    return a tuple of the amount deposited/withdrawn and the remainder.

    Check how much it has using the balance property

    You can also set caps on the minimum and maximum.
    */
    self._amt = initial_reserve
    self.min_cap = min_cap
    self.max_cap = max_cap

    // Callbacks for onchange and onempty
    self.onchange = []
    self.onempty = []
    self.onfull = []

    self.balance = function(){
        // Read only balance
        return self._amt
    }

    self.percent = function(){
        // Read only balance
        return self._amt / (self.max_cap - self.min_cap)
    }

    self.transaction = function(amt){
        // WIthdraw/deposit the specified amount. Returns how much was
        // sucessfully deposited and the leftover
        if (amt === 0){
            return [0.0, 0.0]
        }

        self._amt += amt

        var funct = null
        var remainder = 0

        if (self.max_cap !== null && self._amt >= self.max_cap){
            remainder = self.max_cap - self._amt
            self._amt = self.max_cap
            //Run onfull callbacks
            return [remainder + amt, remainder]
        }

        if (self.min_cap !== null && self._amt <= self.min_cap){
            remainder = self.min_cap - self._amt
            self._amt = self.min_cap
            for (funct in self.onempty){
                self.onempty[funct](self)
            }
            //Run onempty callbacks
            return [remainder + amt, remainder]
        }
        self._run_onchange()
        return [amt, 0.0]
    }

    self._run_onchange = function(){
        for (var i=0; i < self.onchange.length; i++){
            self.onchange[i](self)
        }
    }
}
