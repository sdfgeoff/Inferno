/* globals pc: true */
Inferno.types.Map = function(app) {
    "use strict"
    var self = this
    self.app = app  // Reference to the game used to display the map. This is needed for adding and removing objects

    self.tile_data = null
    self.reverse_tile_data = null
    self.tiles = {}

    self.NUM_SEGMENTS = 40


    self.init = function(){
        self._add_start_room()
        //Creates the starting map ahead of the player
        for (var i = 0; i < Math.floor(self.NUM_SEGMENTS / 2); i++){
            self._add_tile(i+1)
        }
    }

    self.reset = function(){
        for (var t_id in self.tiles){
            var tile_id = parseInt(t_id)
            self._remove_tile(tile_id)
        }
    }


    self.set_player_at = function(tile_entity){
        // Reforms the map so that the player has tiles ahead and behind of them
        var tile_id = null
        for (var t_id in self.tiles){
            if (self.tiles[t_id] == tile_entity){
                tile_id = parseInt(t_id)
            }
        }
        if (tile_id === null){
            return
        }

        var min_id = Math.max(0, tile_id - Math.floor(self.NUM_SEGMENTS * (1/4)))
        var max_id = tile_id + Math.floor(self.NUM_SEGMENTS  * (3/4))
        while (!self.tiles.hasOwnProperty(max_id)){
            self._add_tile(self.get_biggest_tile_id() + 1)
        }

        if (min_id !== 0){
            while (self.tiles.hasOwnProperty(min_id)){
                self._remove_tile(self.get_smallest_tile_id())
            }
        }
    }

    self.get_biggest_tile_id = function(){
        var max_num = 0
        for (var tile in self.tiles){
            max_num = Math.max(max_num, tile)
        }
        return max_num
    }
    self.get_smallest_tile_id = function(){
        var min_num = Infinity
        for (var tile in self.tiles){
            if (tile !== 0){
                min_num = Math.min(min_num, tile)
            }
        }
        return min_num
    }

    self._remove_tile = function(tile_id){
        var tile_obj = self.tiles[tile_id]
        tile_obj.destroy()
        delete self.tiles[tile_id]
    }

    self._add_tile = function(offset){
        // Adds a tile at the specified offset.
        if (self.tiles.hasOwnProperty(offset)){
            return
        }
        var last_tile = self.tiles[offset - 1]
        var last_exit = self.get_exit(last_tile)
        var exit_obj = last_exit[1]
        var exit_data = last_exit[0]
        var new_tile_type = self.pick_tile(exit_data['interface'])

        var transform = exit_obj.getWorldTransform()
        var entity = new pc.Entity(new_tile_type)
        var asset = self.app.assets.find(new_tile_type + '.json')
        self.tiles[offset] = entity

        entity.addComponent("model", {
            type: "asset",
            asset: asset,
            //castShadows: true
        })
        entity.addComponent("collision", {
            type: 'mesh',
            asset: asset,
        })
        entity.addComponent('rigidbody', {
            type: pc.BODYTYPE_STATIC,
            friction: 0.0,
            restitution: 1.0
        })
        entity.setPosition(transform.getTranslation())
        entity.setEulerAngles(transform.getEulerAngles())
        self.app.root.addChild(entity)
    }

    self.get_exit = function(tile){
        var data = self.tile_data[tile.name]
        var exit = data.exit_interfaces[0]
        return [exit, tile.findByName(exit.name)]
    }

    self._add_start_room = function(){
        // Creates the start room
        var entity = new pc.Entity('Lights');
        var asset = self.app.assets.find('Lights' + '.json')
        self.tiles[0] = entity

        entity.addComponent("model", {
            type: "asset",
            asset: self.app.assets.find('Lights' + '.json')
        })
        entity.addComponent("collision", {
            type: 'mesh',
            asset: asset,
        })
        entity.addComponent('rigidbody', {
            type: pc.BODYTYPE_STATIC
        })
        self.app.root.addChild(entity);
        entity.setPosition(0,0,0)
    }

    self.pick_tile = function(interface_type){
        //Returns a string name of a tile with the specified interface ID'''
        var options = self.reverse_tile_data[interface_type]
        return options[Math.floor(Math.random() * options.length)]
    }


    // ----------------------- LOADING GARBAGE -------------------------------
    self._num_assets = 0
    self._asset_load_counter = 0

    self.load = function(loader, percent_callback, done_callback){
        // Loads all the pieces needed for the map. Runs the percent_callback as
        // often as possible, and the done_callback when it is finished
        loader.load_url('tile_data.json', self._load_assets)
        self._loading_percent_callback = percent_callback
        self._loading_done_callback = done_callback
    }

    self._load_assets = function(resource){
        //Runs with a JSON-able string containing a dictionary of tile data
        self.tile_data = JSON.parse(resource)

        //Ensure all interfaces are in the reverse tile data dict
        self.reverse_tile_data = {}
        var data = null
        for (var obj in self.tile_data){
            data = self.tile_data[obj]
            self.reverse_tile_data[data.exit_interfaces[0]['interface']] = []
        }

        //Populate reverse tiledata dict
        var urls = []
        for (var obj in self.tile_data){
            //Interface map it
            urls.push(obj)
            data = self.tile_data[obj]
            var entry_interface = data.entry_interface
            self.reverse_tile_data[entry_interface].push(obj)
        }
        var obj_load_helper = new InfernoObjectListLoader(
            app, "Map",
            urls,
            self._loading_percent_callback, self._loading_done_callback
        )
        obj_load_helper.start()
    }
}
