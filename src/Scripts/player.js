/* globals pc: true */
/* globals Inferno: true */
Inferno.types.Player = function (app, map, texthud){
    "use strict"
    var self = this
    self.app = app
    self.map = map
    self.extras = new Inferno.types.PlayerExtras(self.app)
    self.texthud = texthud

    // Initial, min_cap, seconds of effect
    self.boost = new Inferno.types.Bank(0, 0, 5)
    self.slow = new Inferno.types.Bank(0, 0, 5)

    self.shield = new Inferno.types.Bank(0, 0, 0.3)
    self.dead = false

    self.init = function(){
        self.obj = new pc.Entity('Player')  // The main object that everything
                                            // is associated with
        self.app.root.addChild(self.obj)
        self._set_up_root_obj()
        self.extras.init(self.obj)

        self.boost.onchange.push(self.extras.hud.set_boost_percent)
        self.slow.onchange.push(self.extras.hud.set_slow_percent)
        self.shield.onchange.push(self.extras.hud.set_shield_percent)

        self.boost.onempty.push(self.on_boost_empty)
        self.slow.onempty.push(self.on_slow_empty)
        self.shield.onempty.push(self.on_shield_empty)

        self._mouse_delta = [0, 0]
        self.app.mouse.on(pc.EVENT_MOUSEMOVE, self.onMouseMove)
        self.hit_wall = 0

        self.velocity = 4.0  //How fast the player starts off
    }

    self.reset = function(){
        // Initial, min_cap, seconds of effect
        self.boost = new Inferno.types.Bank(0, 0, 5)
        self.slow = new Inferno.types.Bank(0, 0, 5)

        self.shield = new Inferno.types.Bank(0, 0, 0.3)
        self.dead = false

        if (self.obj){
            self.obj.destroy()
        }
    }

    self.on_shield_empty = function(){
        self.dead = true
    }

    self.update = function(dt){
        //Runs every frame with the delta time in it....
        if (!self.dead){
            var boost = self.app.mouse.isPressed(pc.MOUSEBUTTON_LEFT)
            var slow = self.app.mouse.isPressed(pc.MOUSEBUTTON_RIGHT)

            if (!boost){
                //Seconds of boost / time to recharge
                self.boost.transaction(5.0 / 6.0 * dt)
            }

            if (!slow){
                //Seconds of slow / time to recharge
                self.slow.transaction(5.0 / 15.0 * dt)
            }

            self.shield.transaction(0.3 / 15.0 * dt)
            if (self.hit_wall){
                self.hit_wall = false
                self.shield.transaction(-dt)
            }

            var boost_amt = self.boost.transaction(-dt * boost)[0]
            boost = -boost_amt / dt  // How much boost succeeded in percent
            if (!boost){
                // If the player isn't boosting, make there speed slowly increase
                self.velocity += dt / 15
            }

            boost = boost * (3.0 - 1) + 1  // Scaled properly so that it sits at 1 normally

            var slow_amt = self.slow.transaction(-dt * slow)[0]
            slow = -slow_amt / dt  // How much slowness succeeded in percent
            slow = slow * (0.5 - 1) + 1  // Scaled properly

            var current_tile = self.get_current_tile()
            if (current_tile !== null){
                self.map.set_player_at(current_tile)
            }

            self._update_angular(dt, current_tile)
            self._update_linear(boost, slow, dt)
        }

        // Things that don't depend on the player being alive:
        var lin_vel = self.getLocalLinearVelocity()
        self.extras.hud.show_velocity(lin_vel)
        self.texthud.set_speed(lin_vel)

        self.extras.post_effect.streak_center_x = lin_vel.x / 10.0 + 0.5
        self.extras.post_effect.streak_center_y = -lin_vel.y / 10.0 + 0.5
        self.extras.post_effect.streak_length = lin_vel.z / 3.0

        self.texthud.update()
    }

    self._update_linear = function(boost, slow, dt){
        // Player speed and motion
        var force_vec = new pc.Vec3([0, 0, 0])

        // From the engines
        var delta = boost * slow
        force_vec.z += self.velocity * delta * 300 // 300 to cancel out damping 300

        // From drag
        var lin_vel = self.getLocalLinearVelocity()
        lin_vel.scale(300)
        force_vec.sub(lin_vel)

        // Enact the forces
        force_vec.scale(dt)
        self.applyLocalForce(force_vec)
    }

    self._update_angular = function(dt, current_tile){
        // Player angular speed
        var delta = self._mouse_delta
        self._mouse_delta = [0, 0]

        // Torque from input:
        var torque_vec = new pc.Vec3()
        torque_vec.x -= delta[1]
        torque_vec.y += delta[0]
        torque_vec.z += delta[0] * 0.5 // Player roll factor
        // So the ui sensitivity is ~0-100 we have an extra factor:
        torque_vec.scale(5)

        // Torque from damping:
        var ang_vel = self.getLocalAngularVelocity()
        // self.hud.setAngularIndicator(ang_val)
        ang_vel.scale(30)
        torque_vec.sub(ang_vel)

        // Torque from auto-leveling
        if (current_tile !== null){
            var rot_diff = new pc.Vec3().cross(current_tile.forward, self.obj.forward)
            rot_diff = self.global_to_local(rot_diff)
            torque_vec.z -= rot_diff.z * 100
        }

        // Enact the torques
        torque_vec.scale(dt)
        self.applyLocalTorque(torque_vec)
    }

    self.get_current_tile = function(){
        //Returns a 3vec of the direction "up" is
        var hit_data = null
        var ray_start = self.obj.getPosition()
        var ray_end = new pc.Vec3().copy(self.obj.forward)
        ray_end.scale(5)
        ray_end.add(ray_start)

        hit_data = self.app.systems.rigidbody.raycastFirst(
            ray_start,
            ray_end
        )

        if (hit_data !== null){
            return hit_data.entity
        }
        return null
    }

    self._set_up_root_obj = function(){
        self.obj.addComponent("collision", {
            type: 'sphere',
            radius: 0.25,
        })
        self.obj.addComponent('rigidbody', {
            type: pc.BODYTYPE_DYNAMIC,
            friction: 0.0,
            restitution: 0.0  // We will handle bouncing manually
        })
        self.obj.collision.on('contact', self.on_hit)
    }
    self.on_hit = function(data){
        if (!self.dead){
            var normal = data.contacts[0].normal
            normal.scale(17) //Bounce force
            self.obj.rigidbody.applyForce(normal)
            self.hit_wall = true
        }
    }

    self.applyLocalTorque = function(torque_vec){
        self.obj.rigidbody.applyTorque(self.local_to_global(torque_vec))
    }

    self.getLocalAngularVelocity = function(){
        var ang_vel = self.obj.rigidbody.angularVelocity
        return self.global_to_local(ang_vel)
    }

    self.applyLocalForce = function(torque_vec){
        self.obj.rigidbody.applyForce(self.local_to_global(torque_vec))
    }

    self.getLocalLinearVelocity = function(){
        var lin_vel = self.obj.rigidbody.linearVelocity
        return self.global_to_local(lin_vel)
    }

    self.local_to_global = function(vec){
        var global_vec = new pc.Vec3()
        global_vec.add(self.obj.right.clone().scale(vec.x))
        global_vec.add(self.obj.forward.clone().scale(vec.y))
        global_vec.add(self.obj.up.clone().scale(vec.z))
        return global_vec
    }

    self.global_to_local = function(vec){
        //Converts from global to local
        var local_vec = new pc.Vec3()
        local_vec.x = vec.dot(self.obj.right)
        local_vec.y = vec.dot(self.obj.forward)
        local_vec.z = vec.dot(self.obj.up)
        return local_vec
    }

        self.onMouseMove = function(e){
        self._mouse_delta[0] = e.dx
        self._mouse_delta[1] = e.dy
    }

    self.on_boost_empty = function(){
        self.texthud.setBigText("Boost Drive Empty")
    }
    self.on_slow_empty = function(){
        self.texthud.setBigText("Time Warp Empty")
    }
}
