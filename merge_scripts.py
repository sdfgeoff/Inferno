#!/usr/bin/python
import sys

START_INDICATOR = '<<'
END_INDICATOR = '>>'


def do_string(path):
    raw = open(path[0]).read()
    raw = raw.replace('\n', '",\n"', 100)
    raw = '["' + raw + '"].join("\\n")'
    return raw

def do_file(path):
    return open(path[0]).read()

COMMAND_TABLE = {
    'STRING': do_string,
    'FILE': do_file,
}

def compute_new_str(slug):
    command = slug.split(' ')[0]
    if command in COMMAND_TABLE:
        return COMMAND_TABLE[command](slug.split(' ')[1:])
    print("Command {} not found".format(command))
    return ""

def fix_string(input_string):
    '''Does all the substitutions'''
    replace_str = get_between(input_string, START_INDICATOR, END_INDICATOR)
    while replace_str is not None:
        plain_str = replace_str[len(START_INDICATOR):-len(END_INDICATOR)]
        input_string = input_string.replace(replace_str, compute_new_str(plain_str))

        replace_str = get_between(input_string, START_INDICATOR, END_INDICATOR)

    return input_string

def get_between(inp_str, start_chars, end_chars, start_index=None, end_index=None):
    '''Returns the first chars between two strings segments, inclusive.
    This means you can easily do a string replace and know what will happen'''
    found_index = inp_str.find(start_chars, start_index, end_index)
    if found_index == -1:
        return None
    final_index = inp_str.find(end_chars, found_index)
    if final_index == -1:
        raise Exception("Unterminated")
    return inp_str[found_index:final_index+len(end_chars)]



def console_mode(args):
    '''Runs the parser from the console'''
    if len(args) != 3 or args[1] == 'help':
        print("merge_scripts.py [inp_process_file] [out_process_file]")
        return

    inp_str = open(args[1]).read()
    outp_str = fix_string(inp_str)
    open(args[2], 'w').write(outp_str)


if __name__ == '__main__':
    console_mode(sys.argv)
