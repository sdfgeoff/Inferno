BROWSER = chromium
BLENDER = blender

PLAYCANVAS = bin/Scripts/playcanvas-latest.js
PLAYCANVAS_EXPORT_HELPER = single_model_export.py

MODELS = $(shell find src/Models -name *.blend)
SCRIPTS = $(shell find src/Scripts -name *.js -printf "%f\n")

TILE_BLEND = src/Models/TileSet.blend
TILE_EXPORT_SCRIPT = export_tile_data.py

PROCESS_SCRIPTS_SCRIPT = ./merge_scripts.py


all: complete $(PLAYCANVAS)

$(PLAYCANVAS): ./lib/engine/src/*
	cd ./lib/engine/build && \
	node build.js -l 1 -o ../../../$(PLAYCANVAS)


	#npm install fs-extra && \
	#npm install google-closure-compiler && \
	#npm install preprocessor && \
	# node build.js -l 1 -o ../../../$(PLAYCANVAS)

models: $(MODELS) tiledata
	@# Exports models from blender, puts them in bin/models. The exporter
	@# script automagically deals with textures, copying them to a place
	@# relative to the model directory
	@for model in $(MODELS) ; do \
		echo Processing Model $$model; \
        blender $$model -b --python $(PLAYCANVAS_EXPORT_HELPER); \
    done

scripts:
	@for script in $(SCRIPTS) ; do \
		echo Processing Scripts $$script; \
        $(PROCESS_SCRIPTS_SCRIPT) src/Scripts/$$script bin/Scripts/$$script; \
    done

static-resources: ./src/*
	# Copying Static Resources
	mkdir -p ./bin
	mkdir -p ./bin/Scripts
	mkdir -p ./bin/Sounds
	cp -r ./src/Scripts/* ./bin/Scripts/
	cp -r ./src/Sounds/* ./bin/Sounds/
	cp ./src/*.html ./bin/
	cp ./src/*.css ./bin/
	cp ./src/Textures/*.ttf ./bin/Textures/

	#cp ./lib/ammo.js/builds/ammo.js ./bin/Scripts/ammo.js
	cp ./lib/ammo.*.js ./bin/Scripts/ammo.js

tiledata:
	@# Exports a JSON mapping between tiles
	blender $(TILE_BLEND) -b --python $(TILE_EXPORT_SCRIPT);

quick: static-resources scripts
	# Only copies scripts and static resources, does not build
	# models

complete: static-resources models scripts $(PLAYCANVAS)

test: quick
	python -m http.server & $(BROWSER) 0.0.0.0:8000/bin

clean:
	rm -r ./bin/*

